from django.http import HttpResponseRedirect
from django.urls import include, path

urlpatterns = [
    path("", lambda r: HttpResponseRedirect("/")),
    path(
        "mitel/sip/",
        include("django_pbxdeploy_ven_mitel_sip.django_pbxdeploy_ven_mitel_sip.urls"),
    ),
]
