from django.db import models
from django_pbxdeploy_models.django_pbxdeploy_models.models import *


class ConfigMap:
    def __init__(self):
        self.data = list()

    def add(self, entry: CusDeployPhone):
        for row in self.data:
            if row.phone_var.varid == entry.phone_var.varid:
                return self
        self.data.append(entry)
        return self

    def get(self):
        return self.data

    class Meta:
        managed = False
