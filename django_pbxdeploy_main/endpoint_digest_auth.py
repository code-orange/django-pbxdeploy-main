import python_digest
from django.conf import settings
from tastypie.authentication import DigestAuthentication

from django_pbxdeploy_models.django_pbxdeploy_models.models import PsAuths


class EndpointDigestAuth(DigestAuthentication):
    def __init__(self, **kwargs):
        super(EndpointDigestAuth, self).__init__(**kwargs)

    def is_authenticated(self, request, **kwargs):
        """
        Finds the user and checks their API key.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            self.get_authorization_data(request)
        except ValueError:
            return self._unauthorized()

        digest_response = python_digest.parse_digest_credentials(
            request.META["HTTP_AUTHORIZATION"]
        )

        # FIXME: Should the nonce be per-user?
        if not python_digest.validate_nonce(digest_response.nonce, settings.SECRET_KEY):
            return self._unauthorized()

        user = self.get_user(digest_response.username)

        if user is False:
            return self._unauthorized()

        expected = python_digest.calculate_request_digest(
            request.method,
            python_digest.calculate_partial_digest(
                digest_response.username, self.realm, user.password
            ),
            digest_response,
        )

        if not digest_response.response == expected:
            return self._unauthorized()

        request.phone_auth = user
        return True

    def get_user(self, username):
        try:
            user = PsAuths.objects.get(username=username)
        except (PsAuths.DoesNotExist, PsAuths.MultipleObjectsReturned):
            return False

        return user
